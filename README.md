# HostInfo

The purpose of this project is to export host information from Dynatrace to excel automatically
Current information gathered:
- Hostname
- OS Type
- OS Version

There are currently two versions of this project available:
- API Version
    - Leverages Flask to create an API
    - Must be made internally accessible so the API can be reached
    - Default URL will be: http://localhost:5000/api/v1/HostList
    - A Call can be triggered to the API any time you require a new Host List
- Code Based
    - Must be manually run when required

Both versions export the exact same information

To use this you will be required to ammend/create the following:
- The variable: tenantURL - Please add your Dynatrace tenant URL as a string
- The variable token: Currently looks for an enviroment variable named "api-token"
    - This is so we do not store API keys as plain text within the code
    - Can be ammended on request


**Pre-Reqs**
- Python 3.X installed
- Libraries to install:
    - Flask - If using the API Version
    - Xlsxwriter
    - Requests
    - json
- Dynatrace URL
- Dynatrace API token