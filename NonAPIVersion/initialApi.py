import os
import xlsxwriter
from NonAPIVersion import outboundrequests as ob

tenantURL = "Tenant URL HERE"
token = os.environ.get('api-token')

filename = "HostList.xlsx"
workbook = xlsxwriter.Workbook(filename)
HLSheet = workbook.add_worksheet("Host List")
HLSheet.set_column(0, 2, 80)
HLSheet.set_column(1, 1, 30)
TitleFormat = workbook.add_format({'bold': True, 'border': True, 'center_across': True})
CellFormat = workbook.add_format({'border': True})
row = 0
col = 0
HLSheet.write_row(row, col, ["Host Name", "OS Type", "OS Version"], TitleFormat)
row += 1

HostList = ob.dt_get_requests(tenantURL, token, "HostList", "")
for dict in HostList:
    writeList = []
    writeList.append(dict["displayName"])
    writeList.append(dict["osType"])
    writeList.append(dict["osVersion"])
    HLSheet.write_row(row, col, writeList, CellFormat)

    row += 1

workbook.close()
