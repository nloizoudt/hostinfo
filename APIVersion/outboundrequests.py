import requests
import json


def get_with_headers(api_url, Auth):
    response = ""
    headers = {"Authorization": "Api-Token " + Auth.strip()}
    try:
        response = requests.get(api_url, headers=headers)
        response.raise_for_status()
        if response.status_code == 200:
            return json.loads(response.content.decode('utf-8'))
    except requests.exceptions.RequestException as err:
        return "oops: " + str(err)


def dt_get_requests(URL, token,type, id):
    api_dict = {}
    api_dict["HostList"] = URL + "/api/v1/entity/infrastructure/hosts"
    api_dict["HostInfo"] = URL + "/api/v1/entity/infrastructure/hosts" + id



    response = get_with_headers(api_dict[type], token)
    return response
